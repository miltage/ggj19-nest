package;

import openfl.display.Sprite;
import openfl.display.BitmapData;
import openfl.geom.Point;
import openfl.Assets;
import openfl.Lib;

import spritesheet.AnimatedSprite;
import spritesheet.Spritesheet;
import spritesheet.data.BehaviorData;
import spritesheet.importers.BitmapImporter;

import Entity;

enum PlayerTeam
{
  RED;
  BLUE;
}

class Player extends Entity
{
  public static inline var FRAME_RATE:Int = 15;
  public static inline var RADIUS:Int = 30;
  public static inline var PICKUP_RADIUS:Int = 30;
  public static inline var WHISTLE_TIME:Int = 400;
  public static inline var WHISTLE_COOL_DOWN:Int = 600;

  private var animation:AnimatedSprite;
  private var lastMove:MoveDirection;
  private var carriedItem:Item;

  private var whistling:Bool;
  private var whistleTime:Int;

  public function new()
  {
    super();

    whistling = false;
    whistleTime = 0;

    #if debug
    graphics.beginFill(0xDEFEC8, 1);
    graphics.drawCircle(0, 0, RADIUS);
    #end

    var bitmapData:BitmapData = Assets.getBitmapData("assets/trump_run.png");
    var spritesheet:Spritesheet = BitmapImporter.create(bitmapData, 6, 4, 100, 100);

    spritesheet.addBehavior(new BehaviorData("walk_south", [0, 1, 2, 3, 4, 5], true, FRAME_RATE));
    spritesheet.addBehavior(new BehaviorData("walk_east", [6, 7, 8, 9, 10, 11], true, FRAME_RATE));
    spritesheet.addBehavior(new BehaviorData("walk_north", [12, 13, 14, 15, 16, 17], true, FRAME_RATE));
    spritesheet.addBehavior(new BehaviorData("walk_west", [18, 19, 20, 21, 22, 23], true, FRAME_RATE));

    spritesheet.addBehavior(new BehaviorData("idle_south", [1], true, FRAME_RATE));
    spritesheet.addBehavior(new BehaviorData("idle_east", [7], true, FRAME_RATE));
    spritesheet.addBehavior(new BehaviorData("idle_north", [13], true, FRAME_RATE));
    spritesheet.addBehavior(new BehaviorData("idle_west", [19], true, FRAME_RATE));

    animation = new AnimatedSprite(spritesheet, true);
    animation.x = -50;
    animation.y = -75;
    addChild(animation);

    lastMove = DOWN;
    animation.showBehavior("idle_south");
  }

  public function move(dir:MoveDirection):Void
  {
    velocity = getDirection(dir);

    animation.showBehavior(switch (dir) {
      case UP | UP_LEFT | UP_RIGHT: "walk_north";
      case DOWN | DOWN_LEFT | DOWN_RIGHT: "walk_south";
      case LEFT: "walk_west";
      case RIGHT: "walk_east";
    }, false);

    lastMove = dir;
  }

  public function isCarryingItem():Bool
  {
    return carriedItem != null;
  }

  public function getCarriedItem():Item
  {
    return carriedItem;
  }

  public function carry(item:Item):Void
  {
    carriedItem = item;
    item.setCarried(true);
  }

  public function drop():Void
  {
    if (carriedItem == null) return;

    carriedItem.throwItem(lastMove);
    carriedItem.setCarried(false);
    carriedItem = null;
  }

  public function whistle():Void
  {
    if (whistleTime > 0) return;

    whistleTime = WHISTLE_TIME + WHISTLE_COOL_DOWN;
  }

  public function isWhistling():Bool
  {
    return whistleTime > WHISTLE_COOL_DOWN;
  }

  override public function update(delta:Int):Void
  {
    super.update(delta);

    animation.update(delta);

    if (whistleTime > 0) whistleTime -= delta;

    if (Math.abs(velocity.x) < 0.1 && Math.abs(velocity.y) < 0.1)
    {
      animation.showBehavior(switch (lastMove) {
        case UP | UP_LEFT | UP_RIGHT: "idle_north";
        case DOWN | DOWN_LEFT | DOWN_RIGHT: "idle_south";
        case LEFT: "idle_west";
        case RIGHT: "idle_east";
      }, false);
    }

    if (carriedItem != null)
    {
      carriedItem.x = x;
      carriedItem.y = y + 5;
    }
  }

  override public function collidesWith(entity:Entity):Bool
  {
    return Std.is(entity, Player) || Std.is(entity, ChickenHouse) || Std.is(entity, Item) && cast(entity, Item).blocks();
  }
}