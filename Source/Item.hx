package;

import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.geom.Point;
import openfl.Assets;

import Entity;

enum ItemType
{
  GRASS;
  ROCK;
  BOULDER;
  TREE;
}

class Item extends Entity
{
  public static inline var RADIUS:Int = 25;

  private var carried:Bool;
  private var type:ItemType;
  private var asset:Bitmap;
  private var remove:Bool;

  public function new(type:ItemType)
  {
    super();

    this.type = type;

    carried = false;
    speed = 10;
    remove = false;

    var bitmapData:BitmapData = Assets.getBitmapData(
      switch (type) {
        case ROCK: "assets/rock.png";
        case GRASS: "assets/hay.png";
        case BOULDER: "assets/Boulder.png";
        default: "assets/blank.png";
      });

    asset = new Bitmap(bitmapData);
    asset.x = -asset.width/2;
    asset.y = -asset.height * 0.6;
    addChild(asset);

    #if debug
    graphics.beginFill(0xFECC80, 1);
    graphics.drawCircle(0, 0, RADIUS);
    #end
  }

  public function markForRemoval():Void
  {
    remove = true;
  }

  public function markedForRemoval():Bool
  {
    return remove;
  }

  public function getType():ItemType
  {
    return type;
  }

  public function canLift():Bool
  {
    return switch (type) {
      case GRASS: true;
      case ROCK | TREE | BOULDER: false;
    }
  }

  public function blocks():Bool
  {
    return switch (type) {
      case GRASS: false;
      case ROCK | TREE | BOULDER: true;
    }
  }

  public function throwItem(dir:MoveDirection)
  {
    velocity = getDirection(dir);
  }

  public function isCarried():Bool
  {
    return carried;
  }

  public function setCarried(carried:Bool):Void
  {
    this.carried = carried;
  }
}