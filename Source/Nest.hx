package;

import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.Assets;

class Nest extends Entity
{
  public static inline var RADIUS:Int = 45;

  private var asset:Bitmap;

  public function new()
  {
    super();

    #if debug
    graphics.beginFill(0x5542DC, 1);
    graphics.drawCircle(0, 0, RADIUS);
    #end

    var bitmapData:BitmapData = Assets.getBitmapData("assets/Nest1.png");
    asset = new Bitmap(bitmapData);
    asset.x = -asset.width/2;
    asset.y = -asset.height * 0.6;
    addChild(asset);
  }  
}