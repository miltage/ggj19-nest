package;

import openfl.geom.Point;

class ChickenHouse extends Entity 
{
  public static inline var RADIUS:Int = 40;
  public static inline var WHISTLE_RADIUS:Int = 200;
  public static inline var WANDER_WAIT:Int = 100;
  public static inline var WANDER_TIME:Int = 20;

  private var wanderTime:Int;
  private var wanderDirection:Point;
  private var nesting:Bool;

  public function new()
  {
    super();

    speed = 3;
    wanderTime = Std.int(WANDER_WAIT * Math.random());
    wanderDirection = new Point();
    nesting = false;

    #if debug
    graphics.lineStyle(2, 0xFF2244, 1);
    graphics.drawCircle(0, 0, WHISTLE_RADIUS);
    graphics.lineStyle(0, 0, 0);

    graphics.beginFill(0xE80C7A, 1);
    graphics.drawCircle(0, 0, RADIUS);
    #end
  }

  public function isNesting():Bool
  {
    return nesting;
  }

  public function settleIn(nest:Nest):Void
  {
    nesting = true;
    x = nest.x;
    y = nest.y + 5;
  }

  override public function collidesWith(entity:Entity):Bool
  {
    return Std.is(entity, Player) || Std.is(entity, ChickenHouse) || Std.is(entity, Item) && cast(entity, Item).blocks();
  }

  override public function update(delta:Int):Void
  {
    super.update(delta);

    if (wanderTime > 0)
      wanderTime--;
    else
    {
      wanderDirection = new Point(Math.random() * 2 - 1, Math.random() * 2 - 1);
      wanderTime = WANDER_TIME + WANDER_WAIT + Std.int(Math.random() * WANDER_WAIT);
    }

    if (wanderTime < WANDER_TIME && !nesting)
    {
      velocity.x = wanderDirection.x;
      velocity.y = wanderDirection.y;
    }
  }

}