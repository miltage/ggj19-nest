package;

import openfl.display.Sprite;
import openfl.geom.Point;
import openfl.Lib;

import Entity;
import Player;
import Item;

class Game extends Sprite
{
  private var playerMap:Map<PlayerTeam, Player>;
  private var houseMap:Map<PlayerTeam, ChickenHouse>;
  private var input:InputController;
  private var entities:Array<Entity>;
  private var items:Array<Item>;
  private var nests:Array<Nest>;

  private var container:Sprite;

  private var lastTime:Int;

  public function new(input:InputController)
  {
    super();

    this.input = input;

    lastTime = Lib.getTimer();

    init();
  }

  public function reset():Void
  {
    removeChild(container);
    init();
  }

  private function init():Void
  {
    playerMap = new Map<PlayerTeam, Player>();
    houseMap = new Map<PlayerTeam, ChickenHouse>();
    entities = new Array<Entity>();
    items = new Array<Item>();
    nests = new Array<Nest>();

    container = new Sprite();
    addChild(container);

    var sw = Lib.current.stage.stageWidth;
    var sh = Lib.current.stage.stageHeight;

    {
      var player = new Player();
      playerMap.set(RED, player);
      player.x = 100;
      player.y = 100;
      container.addChild(player);
      entities.push(player);
    }

    {
      var player = new Player();
      player.x = 850;
      player.y = 150;
      playerMap.set(BLUE, player);
      container.addChild(player);
      entities.push(player);
    }

    {
      var house = new ChickenHouse();
      house.x = 400;
      house.y = 100;
      container.addChild(house);
      houseMap.set(RED, house);
      entities.push(house);
    }

    {
      var house = new ChickenHouse();
      house.x = 400;
      house.y = 300;
      container.addChild(house);
      houseMap.set(BLUE, house);
      entities.push(house);
    }

    for (i in 0...5)
    {
      var item = new Item(GRASS);
      item.x = sw * 0.1 + Math.random() * sw * 0.8;
      item.y = sh * 0.1 + Math.random() * sh * 0.8;
      container.addChild(item);
      entities.push(item);
      items.push(item);
    }

    for (i in 0...5)
    {
      var item = new Item(ROCK);
      item.x = sw * 0.1 + Math.random() * sw * 0.8;
      item.y = sw * 0.1 + Math.random() * sh * 0.8;
      container.addChild(item);
      entities.push(item);
      items.push(item);
    }

    for (i in 0...2)
    {
      var item = new Boulder();
      item.x = sw * 0.1 + Math.random() * sw * 0.8;
      item.y = sw * 0.1 + Math.random() * sh * 0.8;
      container.addChild(item);
      entities.push(item);
      items.push(item);
    }
  }

  public function update():Void
  {
    var time = Lib.getTimer();
    var delta = time - lastTime;
    lastTime = time;

    if (getHouse(RED).isNesting() || getHouse(BLUE).isNesting())
      return;

    // collision
    for (i in 0...entities.length)
      for (j in (i + 1)...entities.length)
        if (entities[i].collidesWith(entities[j]) && entities[i].isCollidingWith(entities[j]))
          entities[i].resolveCollision(entities[j]);

    // depth sorting
    for (i in 0...container.numChildren)
    {
      for (j in i...container.numChildren)
      {
        if (container.getChildAt(i).y > container.getChildAt(j).y)
          container.swapChildrenAt(i, j);
      }
    }

    // player movement
    if (input.isKeyDown('W'.code) && input.isKeyDown('A'.code))
      getPlayer(RED).move(UP_LEFT);
    else if (input.isKeyDown('W'.code) && input.isKeyDown('D'.code))
      getPlayer(RED).move(UP_RIGHT);
    else if (input.isKeyDown('S'.code) && input.isKeyDown('A'.code))
      getPlayer(RED).move(DOWN_LEFT);
    else if (input.isKeyDown('S'.code) && input.isKeyDown('D'.code))
      getPlayer(RED).move(DOWN_RIGHT);
    else if (input.isKeyDown('D'.code))
      getPlayer(RED).move(RIGHT);
    else if (input.isKeyDown('A'.code))
      getPlayer(RED).move(LEFT);
    else if (input.isKeyDown('W'.code))
      getPlayer(RED).move(UP);
    else if (input.isKeyDown('S'.code))
      getPlayer(RED).move(DOWN);

    if (input.isKeyDown(38) && input.isKeyDown(37))
      getPlayer(BLUE).move(UP_LEFT);
    else if (input.isKeyDown(38) && input.isKeyDown(39))
      getPlayer(BLUE).move(UP_RIGHT);
    else if (input.isKeyDown(40) && input.isKeyDown(37))
      getPlayer(BLUE).move(DOWN_LEFT);
    else if (input.isKeyDown(40) && input.isKeyDown(39))
      getPlayer(BLUE).move(DOWN_RIGHT);
    else if (input.isKeyDown(39))
      getPlayer(BLUE).move(RIGHT);
    else if (input.isKeyDown(37))
      getPlayer(BLUE).move(LEFT);
    else if (input.isKeyDown(38))
      getPlayer(BLUE).move(UP);
    else if (input.isKeyDown(40))
      getPlayer(BLUE).move(DOWN);

    // whistle
    if (input.isKeyDown('Q'.code))
      getPlayer(RED).whistle();
    if (input.isKeyDown(32))
      getPlayer(BLUE).whistle();

    //  pick up item / drop item
    var closestItem = getClosestItemTo(getPlayer(RED));
    if (input.isKeyPressed('E'.code) && !getPlayer(RED).isCarryingItem() 
      && closestItem.withinDistanceOf(getPlayer(RED), Player.PICKUP_RADIUS)
      && closestItem.canLift())
    {
      getPlayer(RED).carry(getClosestItemTo(getPlayer(RED)));
    }
    else if (input.isKeyPressed('E'.code))
    {
      getPlayer(RED).drop();
    }

    closestItem = getClosestItemTo(getPlayer(BLUE));
    if (input.isKeyPressed('M'.code) && !getPlayer(BLUE).isCarryingItem() 
      && closestItem.withinDistanceOf(getPlayer(BLUE), Player.PICKUP_RADIUS)
      && closestItem.canLift())
    {
      getPlayer(BLUE).carry(getClosestItemTo(getPlayer(BLUE)));
    }
    else if (input.isKeyPressed('M'.code))
    {
      getPlayer(BLUE).drop();
    }

    for (entity in entities)
      entity.update(delta);

    // house respond to whistle
    if (getPlayer(RED).isWhistling() && getHouse(RED).withinDistanceOf(getPlayer(RED), ChickenHouse.WHISTLE_RADIUS))
      getHouse(RED).moveToward(getPlayer(RED));
    if (getPlayer(BLUE).isWhistling() && getHouse(BLUE).withinDistanceOf(getPlayer(BLUE), ChickenHouse.WHISTLE_RADIUS))
      getHouse(BLUE).moveToward(getPlayer(BLUE));

    // house nests
    for (nest in nests)
    {
      if (getHouse(RED).isCollidingWith(nest))
        getHouse(RED).settleIn(nest);
      if (getHouse(BLUE).isCollidingWith(nest))
        getHouse(BLUE).settleIn(nest);
    }

    // building nests
    for (i in 0...items.length)
      for (j in (i + 1)...items.length)
      {
        if (Type.enumEq(items[i].getType(), GRASS) && Type.enumEq(items[j].getType(), GRASS) 
          && items[i].isCollidingWith(items[j]) && !items[i].isCarried() && !items[j].isCarried())
        {
          container.removeChild(items[i]);
          container.removeChild(items[j]);

          var nest = new Nest();
          nest.x = items[i].x;
          nest.y = items[i].y;
          nests.push(nest);
          container.addChild(nest);

          items[i].markForRemoval();
          items[j].markForRemoval();
        }
      }

    for (item in items)
      if (item.markedForRemoval())
        items.remove(item);
  }

  public function getClosestItemTo(entity:Entity):Item
  {
    return getClosestItem(Std.int(entity.x), Std.int(entity.y));
  }

  public function getClosestItem(x:Int, y:Int):Item
  {
    var closest:Item = null;
    var dist:Float = 9000;
    for (item in items)
    {
      var d = Point.distance(new Point(x, y), new Point(item.x, item.y));
      if (d < dist)
      {
        closest = item;
        dist = d;
      }
    }
    return closest;
  }

  public function getPlayer(team:PlayerTeam):Player
  {
    return playerMap.get(team);
  }

  public function getHouse(team:PlayerTeam):ChickenHouse
  {
    return houseMap.get(team);
  }
}