package;

class Boulder extends Item
{
  public static inline var RADIUS:Int = 50;

  public function new()
  {
    super(BOULDER);

    #if debug
    graphics.beginFill(0xFECC80, 1);
    graphics.drawCircle(0, 0, RADIUS);
    #end
  }
  
}